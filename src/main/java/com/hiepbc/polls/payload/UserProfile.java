package com.hiepbc.polls.payload;

import java.time.Instant;

public class UserProfile {
	private Long id;
	private String username;
	private String name;
	private Instant joineAt;
	private Long pollCount;
	private Long voteCount;

	public UserProfile(Long id, String username, String name, Instant joineAt, Long pollCount, Long voteCount) {
		super();
		this.id = id;
		this.username = username;
		this.name = name;
		this.joineAt = joineAt;
		this.pollCount = pollCount;
		this.voteCount = voteCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Instant getJoineAt() {
		return joineAt;
	}

	public void setJoineAt(Instant joineAt) {
		this.joineAt = joineAt;
	}

	public Long getPollCount() {
		return pollCount;
	}

	public void setPollCount(Long pollCount) {
		this.pollCount = pollCount;
	}

	public Long getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(Long voteCount) {
		this.voteCount = voteCount;
	}

}
