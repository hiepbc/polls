package com.hiepbc.polls.model;

public enum RoleName {
	ROLE_USER,
	ROLE_ADMIN
}
